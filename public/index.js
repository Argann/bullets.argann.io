var i = 0;

var counter = document.getElementById("count");
var minusButton = document.getElementById("minus");
var plusButton = document.getElementById("plus");

function refreshCounter()
{
    counter.innerText = ""+i;
}

function plus()
{
    i++;
    refreshCounter();
}

function minus()
{
    if (i > 0)
    {
        i--;
        refreshCounter();
    }
}

minusButton.onclick = minus;

plusButton.onclick = plus;